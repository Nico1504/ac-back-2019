const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");

//Initializing server
const app = express();
app.use(bodyParser.json());
const port = 8080;
app.listen(port, () => {
    console.log("Server online on: " + port);
});

app.use('/' , express.static('../front-end'))

const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "siscit_abonament"
});

connection.connect(function (err) {

    console.log("Connected to database!");
    const sql = "CREATE TABLE IF NOT EXISTS abonament(sex VARCHAR(3),CNP VARCHAR(20),varsta VARCHAR(3),nume VARCHAR(40),prenume VARCHAR(40),telefon VARCHAR(15),email VARCHAR(50),facebook VARCHAR(60),tipAbonament VARCHAR(20),nrCard VARCHAR(40),cvv INTEGER)";
    connection.query(sql, function (err, result) {
        if (err) throw err;
    });
});

app.post("/abonament", (req, res) => {
    let nume = req.body.nume;
    let prenume = req.body.prenume;
    let telefon = req.body.telefon;
    let email = req.body.email;
    let facebook = req.body.facebook;
    let tipAbonament = req.body.tipAbonament;
    let nrCard = req.body.nrCard;
    let cvv = req.body.cvv;
    let varsta=req.body.varsta;
    let CNP=req.body.CNP;
    let sex=req.body.sex;

    let error = []
    if (!nume||!prenume||!telefon||!email||!facebook||!tipAbonament||!nrCard||!cvv||!varsta||!CNP) {
        error.push("Unul sau mai multe campuri nu au fost introduse");
        console.log("Unul sau mai multe campuri nu au fost introduse!");
      } else {
        if (nume.length < 3 || nume.length > 20) {
          console.log("Nume invalid!");
          error.push("Nume invalid");
        } else if (!nume.match("^[A-Za-z]+$")) {
          console.log("Numele trebuie sa contina doar litere!");
          error.push("Numele trebuie sa contina doar litere!");
        }
        if (prenume.length < 3 || prenume.length > 20) {
          console.log("Prenume invalid!");
          error.push("Prenume invalid!");
        } else if (!prenume.match("^[A-Za-z]+$")) {
          console.log("Prenumele trebuie sa contina doar litere!");
          error.push("Prenumele trebuie sa contina doar litere!");
        }
        if (telefon.length != 10) {
          console.log("Numarul de telefon trebuie sa fie de 10 cifre!");
          error.push("Numarul de telefon trebuie sa fie de 10 cifre!");
        } else if (!telefon.match("^[0-9]+$")) {
          console.log("Numarul de telefon trebuie sa contina doar cifre!");
          error.push("Numarul de telefon trebuie sa contina doar cifre!");
        }

        function validateUrl() {
            var pattern = /^(?:(?:http|https):\/\/)?(?:www.)?facebook.com\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[?\w\-]*\/)?(?:profile.php\?id=(?=\d.*))?([\w\-]*)?$/;
            if(pattern.test(facebook)) {
              alert("Correct URL");
            }
            else {
              alert("Incorrect URL");
            }
            return pattern.test(facebook);
          }

        if (!email.includes("@gmail.com") && !email.includes("@yahooo.com")) {
          console.log("Email invalid!");
          error.push("Email invalid!");
        }
        if (nrCard.length != 16) {
            console.log("Numarul cardului trebuie sa fie de 16 cifre!");
            error.push("Numarul cardului trebuie sa fie de 16 cifre!");
          } else if (!nrCard.match("^[0-9]+$")) {
            console.log("Numarul cardului trebuie sa contina doar cifre!");
            error.push("Numarul cardului trebuie sa contina doar cifre!");
         }
        if (cvv.length != 3) {
                console.log("Cvv-ul trebuie sa contina 3 cifre!");
                error.push("Cvv-ul trebuie sa contina 3 cifre!");
         } else if (!cvv.match("^[0-9]+$")) {
                console.log("Cvv-ul trebuie sa contina doar cifre!");
                error.push("Cvv-ul trebuie sa contina doar cifre!");
        }
        if (varsta.length < 1 || varsta.length > 3) {
                    console.log("Campul varsta trebuie sa contina intre 1 si 3 caractere!");
                    error.push("Campul varsta trebuie sa contina intre 1 si 3 caractere!");
             } else if (!varsta.match("^[0-9]+$")) {
                    console.log("Campul varsta trebuie sa contina doar cifre!");
                    error.push("Campul varsta trebuie sa contina doar cifre!");
            }
        if (CNP.length!=13) {
                console.log("CNP trebuie sa contina 13 caractere!");
                error.push("CNP trebuie sa contina 13 caractere!");
         } else if (!CNP.match("^[0-9]+$")) {
                console.log("CNP trebuie sa contina doar cifre!");
                error.push("CNP trebuie sa contina doar cifre!");
         }
         
         
         var n = CNP.toString();
         var prima=n.substr(0,1);
         prima=parseInt(prima);//prima cifra
         
         var anul=n.substr(1,2 );
         anul = parseInt(anul) ;//anul din cnp
         
         var luna=n.substr(3,2);
         luna=parseInt(luna);//luna din cnp
         
         var ziua=n.substr(5,2);
         ziua=parseInt(ziua);//ziua din cnp
         
         
         var today = new Date();
         var dd = String(today.getDate()).padStart(2, '0');
         var mm = String(today.getMonth() + 1).padStart(2, '0');
         var yyyy = today.getFullYear();
         
         if(prima ===1|| prima === 2)
           anul=1900+anul;
         else if (prima ===5 || prima===6)
           anul=2000+anul;
         
         if(yyyy-anul=== varsta)
           {
               if(mm-luna <0)
               { console.log("CNP-ul nu corespunde cu varsta!");
               error.push("CNP-ul nu corespunde cu varsta!");
               }
               else if(mm-luna===0)
                   {if(zz-ziua<0)
                       {console.log("CNP-ul nu corespunde cu varsta!");
                       error.push("CNP-ul nu corespunde cu varsta!");
                       }}
             }
         else if(yyyy-anul< varsta)
           {console.log("CNP-ul nu corespunde cu varsta!");
           error.push("CNP-ul nu corespunde cu varsta!");
           }
         else if(yyyy-anul>varsta)
                  {console.log("CNP-ul nu corespunde cu varsta!");
                  error.push("CNP-ul nu corespunde cu varsta!");
                  }
        
         if(prima%2===1)
            sex='m';
            else sex='f';
      

        
        
                }
      

    if (error.length === 0) {
        const sql =
            "INSERT INTO abonament (nume,prenume,telefon,email,facebook,tipAbonament,nrCard,cvv,CNP,varsta ) VALUES ('" +nume +"','" +prenume +"','" +telefon +"','" +email +"','" +facebook +"','" + tipAbonament +"','" + nrCard +"','" +cvv+"','"+CNP+"','"+varsta+"')";
        connection.query(sql, function (err, result) {
            if (err) throw err;
            console.log("Abonament achizitionat!");
        });

        res.status(200).send({
            message: "Abonament achizitionat!"
        });
        console.log(sql);
    } else {
        res.status(500).send(error)
        console.log("Eroare la inserarea in baza de date!");
    }
});